#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED
#include <math.h>

#define max(a,b) \
	({ __typeof__ (a) _a = (a); \
	  __typeof__ (b) _b = (b); \
	  _a > _b ? _a : _b; })

#define min(a,b) \
	({ __typeof__ (a) _a = (a); \
	  __typeof__ (b) _b = (b); \
	  _a < _b ? _a : _b; })

#define PI 3.1415926

float
randrf(float min, float max)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = max - min;
	float offset = random * diff;
	return min + offset;
}
#endif
