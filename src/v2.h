#ifndef V2_H_INCLUDED
#define V2_H_INCLUDED
#include <math.h>

struct {
	float x;
	float y;
} typedef v2;

float
v2_dist(v2 a, v2 b)
{
	return sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}

float
v2_sqrdist(v2 a, v2 b)
{
	return (b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y);
}

int
v2_cmp(v2 a, v2 b)
{
	if (a.x == b.x && a.y == b.y)
		return 0;
	else
		return 1;
}
#endif

