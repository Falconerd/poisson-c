#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "./v2.h"
#include "./math.h"
#include "./list.h"

int
main(void)
{
	srand(time(NULL));
	int width = 1280;
	int height = 720;
	int n = 2;
	int r = 20;
	int k = 30;
	float w = r / sqrt(n);
	int cols = ceil(width / w);
	int rows = ceil(height / w);

	// Step 0
	v2 *grid = calloc(cols * rows,sizeof(*grid));

	// Step 1
	v2 pos = { randrf(0, width), randrf(0, height) };
	int p = floor(pos.x / w);
	int q = floor(pos.y / w);
	grid[p + q * cols] = pos;
	List active = {NULL, sizeof(v2), 0};
	list_add(&active, &pos);

	// Step 2
	while (active.length > 0) {
		int active_index = rand() % active.length;
		v2 *active_point = list_get_by_index(&active, active_index)->data;

		int found = 0;
		for (int i = 0; i < k; ++i) {
			float angle = rand() * 2 * PI;
			float dist = randrf(r, 2 * r);
			float x = active_point->x + dist * sin(angle);
			float y = active_point->y + dist * cos(angle);
			v2 sample = { x, y };

			if (x < 0 || y < 0 || x > width || y > height)
				continue;

			int col = floor(sample.x / w);
			int row = floor(sample.y / w);
			int p1 = max(0, col - 2);
			int p2 = min(cols - 1, col + 2);
			int q1 = max(0, row - 2);
			int q2 = min(rows - 1, row + 2);

			int collision = 0;

			for (int p = p1; p <= p2; ++p) {
				for (int q = q1; q <= q2; ++q) {
					int index = p + q * cols;
					v2 point = grid[index];
					if (!point.x && !point.y)
						continue;

					if (0 == v2_cmp(point, sample))
						continue;

					if (v2_dist(point, sample) < r) {
						collision = 1;
						break;
					}
				}
			}

			if (!collision) {
				int index = col + row * cols;
				grid[index] = sample;
				list_add(&active, &sample);
				found = 1;
				break;
			}
		}

		if (!found)
			list_remove(&active, active_point);
	}

	for (int i = 0; i < cols * rows; ++i)
		printf("[ %0.f, %0.f ], ", grid[i].x, grid[i].y);
}
