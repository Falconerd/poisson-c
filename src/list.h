#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED
#include <stdlib.h>
#include <string.h>

typedef struct {
	void *head;
	size_t data_size;
	size_t length;
} List;

typedef struct list_item ListItem;

struct list_item {
	ListItem *next;
	void *data;
};

void
list_add(List *list, void *data)
{
	ListItem *tmp = list->head;
	ListItem *item = malloc(sizeof(*item));
	item->data = malloc(list->data_size);
	memcpy(item->data, data, list->data_size);
	list->head = item;
	item->next = tmp;
	list->length++;
}

void
list_remove(List *list, void *data)
{
	ListItem *item = list->head;
	ListItem *prev = NULL;

	while (NULL != item) {
		if (0 == memcmp(data, item->data, list->data_size)) {
			if (NULL == prev) {
				list->head = item->next;
			} else {
				prev->next = item->next;
			}
			free(item->data);
			free(item);
			list->length--;
			break;
		} else {
			prev = item;
			item = item->next;
		}
	}
}

ListItem *
list_get_by_index(List *list, int index)
{
	ListItem *item = list->head;
	int i = 0;

	while (NULL != item) {
		if (i++ == index)
			return item;
		else
			item = item->next;
	}

	return NULL;
}
#endif

